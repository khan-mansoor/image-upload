require 'test_helper'

class MediaControllerTest < ActionDispatch::IntegrationTest
  test "should get save" do
    get media_save_url
    assert_response :success
  end

  test "should get list" do
    get media_list_url
    assert_response :success
  end

end
