class MediaController < ApplicationController
  before_action :verify_auth_token
  skip_before_action :verify_authenticity_token

  def upload
    if @current_user.is_allowed_to_upload?
      @media = Media.new(media_params)
      @media.user = @current_user
      if @media.save
        render json: {status: :created, message: 'Successfully uploaded image file.' }
      else
        render json: {status: :unprocessable_entity, message: "Error uploading file due to: #{@media.errors}"}
      end
    else
      render json: {status: :forbidden, message: "Unable to upload media. You have reached the max number of file uploads."}
    end
  end

  def list
    if @current_user
      media = @current_user.medias
      render json: {status: :ok, media: media }
    else
      render json: {status: :bad_request }
    end
  end

  def media_params
    params.require(:media).permit(:image, :title , :description)
  end

  def verify_auth_token
    authenticate_or_request_with_http_token do |token, options|
      @current_user = User.find_by(
          auth_token: token,
          )
      @current_user.present? ? true : false
    end
  end

end
