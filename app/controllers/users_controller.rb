class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      render json: { status: :created, auth_token: @user.auth_token }
    else
      render json: { status: :unprocessable_entity, error: @user.errors }
    end
  end

  private
    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:username, :password, :auth_token)
    end
end
