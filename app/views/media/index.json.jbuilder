json.array!(@media) do |media|
  json.extract! media, :id, :name, :description
  json.url media_url(media, format: :json)
end