require 'rails_helper'

RSpec.describe MediaController, type: :controller do
  let(:user) { FactoryBot.create(:user, password: 'ABCdef5%') }
  describe '#Media' do
    it 'should return error on upload image file when required fields are missing' do
      media_params = {media: {image: '', title: '', description: ''}}
      request.headers['Authorization'] = "Token #{user.auth_token}"
      post :upload, params: media_params, format: :json
      response_body = JSON.parse response.body
      expect(response).to have_http_status :ok
      expect(response_body['message']).to include("Error uploading file due to:")
    end

    it 'should successfully upload image file' do
      file = fixture_file_upload('test_image.jpeg', 'image/jpeg')
      media_params = {media: {image: file, title: 'Image 1', description: 'Description 1'}}
      request.headers['Authorization'] = "Token #{user.auth_token}"
      post :upload, params: media_params, format: :json
      response_body = JSON.parse response.body
      expect(response).to have_http_status :ok
      expect(response_body['message']).to eq("Successfully uploaded image file.")
      expect(Media.first.image.url).to_not be_nil
    end
  end
end
