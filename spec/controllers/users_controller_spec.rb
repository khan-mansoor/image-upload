require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe '#User' do
    it 'should create user and return Auth Token' do
      user_params = {user: {username: 'User1', password: 'ABCdef$5'}}
      post :create, params: user_params, format: :json
      response_body = JSON.parse response.body
      expect(response).to have_http_status :ok
      expect(response_body['status']).to eq("created")
      expect(response_body['auth_token']).to_not be_nil
    end
    it 'should not create user' do
      user_params = {user: {username: 'User1', password: 'ABCdef'}}
      post :create, params: user_params, format: :json
      response_body = JSON.parse response.body
      expect(response).to have_http_status :ok
      expect(response_body['status']).to eq("unprocessable_entity")
      expect(response_body['auth_token']).to be_nil
    end
  end
end
