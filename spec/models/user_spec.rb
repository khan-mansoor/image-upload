require 'rails_helper'

RSpec.describe User, :type => :model do
  describe 'password validations' do
    it { should validate_presence_of(:username) }
    it { should validate_uniqueness_of(:username) }
  end

  describe 'password validations' do
    subject { build(:user) }
    it { should validate_presence_of(:username) }
    it {should allow_value('Abc!2312')
                   .for(:password)
    }
    it {should_not allow_value('Abc2312')
                   .for(:password)
                   .with_message(/must be 8 digits containing at least one uppercase letter, one lowercase letter, one digit and one special character/)
    }
  end
  
  describe 'Auth token validations' do
    subject { build(:user, password: 'ACS1a!23') }
    it { is_expected.to callback(:generate_token).before(:create) }
  end
  
end